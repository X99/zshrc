
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZSH="/Users/x99/.oh-my-zsh"

ZSH_THEME="powerlevel10k/powerlevel10k"

DISABLE_MAGIC_FUNCTIONS=true

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  zsh-completions
  zsh-syntax-highlighting
  zsh-autosuggestions
  copyfile
  history
  pod
)

source $ZSH/oh-my-zsh.sh

POWERLEVEL9K_MODE='nerdfont-complete'

upgrade() {
	brew upgrade
	brew cu -ay
	brew cleanup
	softwareupdate -ira
}

zget() {
	if [ ! -d ~/Documents/Repositories/zshrc ]; then
		git clone https://gitlab.com/X99/zshrc.git ~/Documents/Repositories/zshrc
		cd ~/Documents/Repositories/zshrc
	else
		cd ~/Documents/Repositories/zshrc
		git pull
	fi
	cp -f ~/Documents/Repositories/zshrc/.zshrc ~
	cp -f ~/Documents/Repositories/zshrc/.p10k.zsh ~
	source ~/.zshrc
	source ~/.p10k.zsh
}

zpush() {
	cd ~/Documents/Repositories/zshrc
	cp -f ~/.zshrc .
	cp -f ~/.p10k.zsh
	git add .
	git commit -m "Autobackup"
	git push
}

# Create a new directory and enter it
function mk() {
	mkdir -p "$@" && cd "$@"
}

alias l='exa -lh --icons'
alias bcay='brew cu -ay'
alias sira='softwareupdate -ira'
alias mu='mas upgrade'
alias bs='brew search'
alias bi='brew install'
alias br='brew remove'
alias bci='brew cask install'
alias bcr='brew cask remove'
alias b='brew'

#activate ZSH-completions
fpath=(/usr/local/share/zsh-completions $fpath)

# the fuck activation
eval $(thefuck --alias)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export PATH="/usr/local/sbin:$PATH"

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"

# Load pyenv into the shell by adding
# the following to ~/.zshrc:
eval "$(pyenv init -)"

export PATH="/usr/local/sbin:$PATH"
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

eval "$(mcfly init zsh)"

export PATH="/Applications/ARX.app/Contents/bin:$PATH"
